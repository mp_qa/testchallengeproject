package challenge;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TestPage {
    @FindBy(xpath = "//input[@type='submit']")
    private WebElement submitButton;
    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameInput;
    @FindBy(xpath = "//ul[@class='values-description t10']")
    private WebElement valuesDescription;
    
    private WebDriver driver;

    public TestPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public TestPage openTestPage(String url) {
        driver.get(url);
        return this;
    }

    public TestPage checkValue(String expectedValue) {
        //If we want to do few tests in one browser session, we need to create check in with <list>
        String currentValue = valuesDescription.getText();
        Assert.assertEquals(expectedValue, currentValue);
        return this;
    }

    public TestPage clickSubmitButton() {
        submitButton.click();
        return this;
    }

    public TestPage sendKeysInFirstName(String keysToSend) {
        firstNameInput.sendKeys(keysToSend);
        return this;
    }

}