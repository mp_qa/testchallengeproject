package testChallenge;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class Preconditions {
    protected WebDriver driver;
    public String testUrl;

    @Before
    public void setUpBrowser() {
        System.setProperty("webdriver.chrome.driver","chromedriver.exe");
        driver = new ChromeDriver();
        testUrl = "http://testingchallenges.thetestingmap.org/index.php";
    }
    @After
    public void closeBrowser () {
        driver.quit();
    }
}
