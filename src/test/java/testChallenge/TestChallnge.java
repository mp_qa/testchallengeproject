package testChallenge;

import challenge.TestPage;
import org.junit.Test;

/*
    created by persianov.m 12.06.2020
 */

public class TestChallnge extends Preconditions {

    @Test
    public void emptyValue (){
        TestPage testPage = new TestPage(driver); //i did not have a time to refactor this
        testPage.openTestPage(testUrl)
            .clickSubmitButton()
            .checkValue("Empty value");
    }

    @Test
    public void spaceValue (){
        TestPage testPage = new TestPage(driver);
        testPage.openTestPage(testUrl)
            .sendKeysInFirstName(" ") //Specify a check here for the field "First Name"
            .clickSubmitButton()
            .checkValue("Space"); //Specify a check here fro expected value
    }

    //Other checks can be written by analogy


}
